#include <stdint.h>

uint32_t roman_num_val(char c){ 
    switch (c) { 
        case 'I': return 1;     
        case 'V': return 5;     
        case 'X': return 10;    
        case 'L': return 50;    
        case 'C': return 100;   
        case 'D': return 500;   
        case 'M': return 1000; 
    } 
    return 0; 
}

__asm uint32_t roman_to_dec(char *s){ 
    extern roman_num_val
    PUSH    {r4-r6, lr}
    MOV     r2, r0
    ; r6 present sum
    MOV     r6, #0
    ; r3 present i
    MOV     r3, #0
    B       for_judge
for_loop
    LDRB    r0, [r2,r3]
    BL.W    roman_num_val
    ; r4 present x
    MOV     r4, r0
    ADDS    r1, r3, #1
    LDRB    r0, [r2,r1]
    BL.W    roman_num_val
    ; r5 present y
    MOV     r5, r0
    CMP     r4, r5
    ; large or equal
    BCS     if_else
    ; less
    SUBS    r0, r6, r4
    B       if_end
if_else
    ADDS    r0, r6, r4
if_end
    MOV     r6, r0
    ; for increase
    ADDS    r3, r3, #1
for_judge
    LDRB    r0, [r2,r3]
    CMP     r0, #0
    BNE     for_loop
    ; return func
    MOV     r0, r6
    POP     {r4-r6, pc}
}

int main(void)
{
    // create a string of roman numerals
    char s[] = "XXIII";
    // run the assembly function
    uint32_t v = roman_to_dec(s);

    while (1)
        ;
}

//*****updated by Matthew Tang in March 2021***************
//*****ARM University Program Copyright © ARM Ltd 2016*****
